package asv.postagger.database;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Test;

public class ImportTSVTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		ImportTSV itsv = new ImportTSV();
		
		try {
			itsv.readTSV();
		} catch (ClassNotFoundException e) {
			System.out.println("1");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("2");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("3");
			e.printStackTrace();
		}
	}

}

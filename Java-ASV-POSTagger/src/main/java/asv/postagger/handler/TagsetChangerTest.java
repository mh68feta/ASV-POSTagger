package asv.postagger.handler;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

public class TagsetChangerTest {

	TagsetChanger tc = new TagsetChanger();

	@Test
	public void test() throws IOException {
		tc.change("examples/tagged.txt", "examples/tagchange.txt");
		
		FileReader file = new FileReader("examples/tagged.txt.NEW");
		BufferedReader reader = new BufferedReader(file);
		
		String line = reader.readLine();
		
	 	assertEquals(line, "Dies/11 ist/33 schnell/22");
	}
}

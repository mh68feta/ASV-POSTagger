package asv.postagger.config;

/**
 * 
 * Config file for data base psw and more
 * // Input: String path, String databaseUrl, String user,
		// String pw, String databaseName, String column, String delimiter
 * @author marvin
 *
 */
public class Config {
	
	public final static String user = "tmprak";
	
	public final static String password = "tmprak";
	
	public final static String myDriver = "com.mysql.cj.jdbc.Driver";
	
	public final static String myUrl = "localhost/tmprak?useSSL=false";
	
    public final static String tableName = "deu_news_2011";
	
	public final static String column = "sentence_tagged";
	
	public final static String delimiter = "\\|" ;
	
    public final static String path = "german_tagged";
    
    public final static float rate = 0.1f;
    
    public final static int limit = 2000000;
    
    public final static boolean onlyWriteFile = true;
    
}

//localhost/tmprak?useSSL=false
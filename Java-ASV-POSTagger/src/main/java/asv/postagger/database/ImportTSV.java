package asv.postagger.database;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import com.opencsv.CSVReader;
import asv.postagger.config.Config;

public class ImportTSV {
	
	//POJO?
	public void readTSV() throws ClassNotFoundException, SQLException, IOException {
		
		Class.forName(Config.myDriver);
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/test", Config.user, Config.password);
		
		conn.createStatement().executeUpdate("drop table testtable");
		conn.createStatement().executeUpdate("CREATE TABLE `test`.`testtable` ("+
											"`id` INT NOT NULL,"+
											"`content` VARCHAR(45) NULL,"+
											"PRIMARY KEY (`id`));");
		
		// the mysql insert statement
		String query = " insert into testtable (id, content) values (?, ?)";

		// reading CSV
		CSVReader reader = new CSVReader(new FileReader("../data/small.tsv"), '\t', '"', 0);
		
		List<String[]> all = reader.readAll();
		
		for(String[] line : all) {
			System.out.println(Arrays.toString(line));
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, Integer.parseInt(line[0]));
			preparedStmt.setString(2, line[1]);

			// execute the preparedstatement
			preparedStmt.execute();
		}
		reader.close();
	}

}

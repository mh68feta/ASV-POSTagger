package asv.postagger;

import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.junit.Test;

import asv.postagger.config.Config;

public class ImportTest {

	@Test
	public void test() {

		//Logger.getGlobal().getParent().getHandlers()[0].setLevel(Level.FINER);
		Logger log = Logger.getLogger("Import");
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new SimpleFormatter());
		log.addHandler(handler);
		Import i = new Import();
		
		i.write_train_file_from_DB("../data/train", Config.myDriver, "localhost/asv_pos_tagger", "root", "1234", "sentence_tagged", "sentence", "\\|", 0.1f, 100000);
	}

}

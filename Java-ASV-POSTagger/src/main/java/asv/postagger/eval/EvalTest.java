package asv.postagger.eval;

import java.io.IOException;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import asv.postagger.handler.RAWFormater;

public class EvalTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() throws IOException {
		RAWFormater rf = new RAWFormater();
		
		rf.removeTags("sentence.txt.TAGGED", "sentence_without.txt", "/");
	}
	
	@Test
	public void traintest() throws IOException {
		RAWFormater rf = new RAWFormater();
		
		rf.train("../../../data/train1k");
	}
	
	@Test 
	public void taggertest() throws IOException {
		
		RAWFormater rf = new RAWFormater();
		
		rf.tagg("../../../data/train.txt.RDR","../../../data/train.txt.DICT","../../../data/sentence.txt");
	}

	@Test
	public void evaltest() throws IOException {
		
		RAWFormater rf = new RAWFormater();
		
		HashMap<String, Long> result = rf.eval("../data/sentence_tagged", "../data/sentence_test");
		
		System.out.println(result.get("false")+"/"+result.get("all"));
	}
	
}

package asv.postagger.cli;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import asv.postagger.Import;
import asv.postagger.config.Config;
import asv.postagger.handler.RAWFormater;

public class Main {

	// TODO: Training, path relative & absolute
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Import i = new Import();
		RAWFormater rf = new RAWFormater();
		Logger log = Logger.getLogger("Import");
		Logger log2 = Logger.getLogger("RAWFormater");
		ConsoleHandler handler = new ConsoleHandler();
		if (Arrays.asList(args).contains("-log")) {
			Logger.getGlobal().getParent().getHandlers()[0].setLevel(Level.FINER);
		}
		String path, databaseUrl, user, pw, tableName, column, delimiter, driver = Config.myDriver;
		int limit;
		float testrate;
		handler.setFormatter(new SimpleFormatter());
		log.addHandler(handler);
		log2.addHandler(handler);

		System.out.println("====== ASV POS TAGGER TRAINING ======");

		if (!Arrays.asList(args).contains("-input")) {
			path = Config.path;
			databaseUrl = Config.myUrl;
			user = Config.user;
			pw = Config.password;
			tableName = Config.tableName;
			column = Config.column;
			delimiter = Config.delimiter;
			testrate = Config.rate;
			limit = Config.limit;
		} else {

			// Input: String path, String databaseUrl, String user,
			// String pw, String tableName, String column, String delimiter
			System.out.println("====== Input: ======\n");
			System.out.println("Path to trainfile:");
			path = in.nextLine();
			System.out.println("Adress of the MySQL database:");
			databaseUrl = in.nextLine();
			System.out.println("String MySQL username:");
			user = in.nextLine();
			System.out.println("MySQL-User password:");
			pw = in.nextLine();
			System.out.println("Table:");
			tableName = in.nextLine();
			System.out.println("Column with tagged sentences:");
			column = in.nextLine();
			System.out.println("Number of Rows you want to use:");
			limit = Integer.parseInt(in.nextLine());
			System.out.println("Token/Tag delimiter:\n" + "(Caution: Special Java String characters need Escaping! "
					+ "For example: input '\\|' instead of '|')");
			delimiter = in.nextLine();
			System.out.println("Percentage of Data to use for testing:");
			testrate = Float.parseFloat(in.nextLine()) / 100;
			in.close();
		}

		try {
			System.out.println("\n====== Writing Training-File ======");
			System.out.println("\nWriting Training-File: " + path + "\nand Test-File:         " + path + "_test");
			i.write_train_file_from_DB(path, driver, databaseUrl, user, pw, tableName, column, delimiter, testrate,
					limit);

			if (!Config.onlyWriteFile) {
				System.out.println("\n====== Training ======");
				System.out.println("\nTraining on Training-File...");
				rf.train("../../../ASVPOSTagger/" + path);

				System.out.println("\n====== Testing ======");
				// Remove Tags
				System.out.println("\nGenerating Test Data...");
				rf.removeTags(path + "_test", path + "_wot", "/");

				// Testing
				System.out.println("\nTagging Test Data...");
				rf.tagg("../../../ASVPOSTagger/" + path + ".RDR", "../../../ASVPOSTagger/" + path + ".DICT",
						"../../../ASVPOSTagger/" + path + "_wot");

				// Eval
				System.out.println("\nEvaluating...");
				HashMap<String, Long> result = rf.eval(path + "_wot.TAGGED", path + "_test");
				System.out.println("\n" + result.get("false") + "/" + result.get("all") + " Fehlerhaft");
				float f = result.get("false"), a = result.get("all");
				float rate = 100 - (f / a) * 100;
				System.out.println("=> Genauigkeit: " + rate + "%");
			}

			System.out.println("====== Done! ======");

		} catch (Exception e) {
			System.out.println("An Error occured! Please try again.");
			if (Arrays.asList(args).contains("-log")) {
				e.printStackTrace();
			}
		}

	}

}

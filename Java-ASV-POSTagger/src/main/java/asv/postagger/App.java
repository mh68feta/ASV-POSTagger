package asv.postagger;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Test;

import asv.postagger.config.Config;
import asv.postagger.handler.RAWFormater;

/**
 * Hello world!
 *
 */
public class App {

	public static void removeTagsTest() throws IOException {
		RAWFormater rf = new RAWFormater();

		rf.removeTags("../data/test/train100k_test", "../data/test/train100k_wot", "/");
	}

	public static void taggertest() throws IOException {

		RAWFormater rf = new RAWFormater();

		rf.tagg("../../../data/train100k.RDR", "../../../data/train100k.DICT", "../../../data/test/train100k_wot");
	}

	public static void importTest() {
		Import im = new Import();
		
		im.write_train_file_from_DB("../data/test/train100k", Config.myDriver, "DBURL",  "root", "1234", "discounts", "sentence", "\\|", 0.1f, 10000000);
	}
	
	public static void evaltest() throws IOException {

		RAWFormater rf = new RAWFormater();

		HashMap<String, Long> result = rf.eval("../data/test/train100k_wot.TAGGED", "../data/test/train100k_test");

		System.out.println(result.get("false") + "/" + result.get("all"));
		
		float f = result.get("false");
		float a = result.get("all");
		
		float rate = 100-(f/a)*100;
		
		System.out.println(rate+"%");
	}
	
	@Test
	public void runTest() throws IOException {
//		importTest();
//		removeTagsTest();
//		taggertest();
		evaltest();
	}
}

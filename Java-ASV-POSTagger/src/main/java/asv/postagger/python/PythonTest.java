package asv.postagger.python;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.BeforeClass;
import org.junit.Test;

public class PythonTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() throws IOException {

			String prg = "import sys\nprint int(sys.argv[1])+int(sys.argv[2])\n";
			BufferedWriter out = new BufferedWriter(new FileWriter("test1.py"));
			out.write(prg);
			out.close();

			ProcessBuilder pb = new ProcessBuilder("python","RDRPOSTagger.py");
			pb.directory(new File("../Libs/RDRPOSTagger/pSCRDRtagger/"));
			Process p = pb.start();

			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String ret;
			
			while( null != (ret = in.readLine())) {
				System.out.println(ret);
			}
			System.out.println(ret);
	
	}

}

# ASV-POSTagger

git clone https://github.com/datquocnguyen/RDRPOSTagger.git

wget http://nlp.stanford.edu/software/stanford-postagger-2015-12-09.zip

## Genauigkeitsergebnisse:

### RDRPOSTagger

*deu_news_2011*:
 
 - 100K (Dez 2016): 96,0410% (-> 1 Satz / Zeile)
 - 1Mio (Jan 2017): 91,6197% (-> 1 Wort / Zeile)
 
 => 1 Mio mit Satz/Zeile testen!
 
 Still alive